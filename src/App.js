import React, { Component } from 'react';
import { Switch,Route} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from './Components/Navbar.js'
import ProductList from './Components/ProductList.js'

import Cart from './Components/Cart.js'
import Default from './Components/Default.js'
import Details from './Components/Details.js'






class App extends Component {
  render() {
    return (
     <React.Fragment>

     <Navbar />
     <Switch>
     	<Route exact path="/" component={ProductList} />
     	<Route path="/cart" component ={Cart} />
     	<Route path="/details" component={Details} />
     	
     	<Route component={Default} />

     </Switch>


     </React.Fragment>
     )
  }
}

export default App;
